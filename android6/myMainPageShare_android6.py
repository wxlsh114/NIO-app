#test purpose : verify the main features on XiaoMi
#os: Android
#device: Mi Redmi Note3
#version:android 6.0.1
#author: Sam Wang
#update date: created by Sam [2018-07-23]

#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestReportEN import HTMLTestRunner
#from appium.webdriver.common.touch_action import TouchAction

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class weilai_test(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['automationName'] = 'UIAutomator2'
        desired_caps['deviceName'] = 'kenzo'
        #desired_caps['udid'] = 'HMKNW17225011700'
        desired_caps['app'] = PATH('../weilai_test.apk')
        desired_caps['appPackage'] = 'cn.com.weilaihui3'
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        desired_caps['noReset'] = True

        self.driver = webdriver.Remote('http://127.0.0.1:4725/wd/hub', desired_caps)
        sleep(3)

    def tearDown(self):
        # end the session
        self.driver.quit()

    def myMainPageShare(self):
        driver=self.driver
        print('TC_检查手机号码登录APP，检查点:我的个人主页的分享方式----微信好友、朋友圈、新浪微博、我的朋友')
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n我的_个人主页的分享方式----开始:'+now)
        sleep(2)
        #我的
        driver.find_element_by_android_uiautomator('new UiSelector().text("我的")').click()
        sleep(2)
        driver.find_element_by_id('cn.com.weilaihui3:id/my_head_info_user_name').click()
        sleep(2)
        #分享
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_opt_icon').click()
        sleep(1)
        #分享微信好友
        driver.find_element_by_android_uiautomator('new UiSelector().text("微信好友")').click()
        sleep(2)
        driver.find_element_by_android_uiautomator('new UiSelector().text("王小龙")').click()
        sleep(1)
        msg=driver.find_element_by_id('com.tencent.mm:id/ank')
        msg.click()
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        msg.send_keys('Test:'+now)
        sleep(1)
        driver.find_element_by_android_uiautomator('new UiSelector().text("分享")').click()
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_weChat_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(1)
        driver.find_element_by_xpath('//android.widget.Button[@text="返回蔚来"]').click()
        sleep(2)
        #分享
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_opt_icon').click()
        sleep(1)
        #分享朋友圈
        driver.find_element_by_android_uiautomator('new UiSelector().text("朋友圈")').click()
        sleep(2)
        msg2=driver.find_element_by_id('com.tencent.mm:id/djv')
        msg2.click()
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        msg2.send_keys('Test:'+now)
        sleep(1)
        #发表
        driver.find_element_by_id('com.tencent.mm:id/hh').click()
        sleep(2)
        #分享
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_opt_icon').click()
        sleep(1)
        #分享新浪微博
        driver.find_element_by_android_uiautomator('new UiSelector().text("新浪微博")').click()
        sleep(6)
        #发送
        driver.find_element_by_id('com.sina.weibo:id/titleSave').click()
        sleep(2)
        #分享
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_opt_icon').click()
        sleep(1)
        #分享我的朋友
        driver.find_element_by_android_uiautomator('new UiSelector().text("我的朋友")').click()
        sleep(2)
        driver.find_element_by_id('cn.com.weilaihui3:id/share_name').click()
        sleep(2)
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_back_icon').click()
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n我的_个人主页的分享方式----结束:'+now)

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(weilai_test('myMainPageShare'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_myMainPageShare_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='蔚来汽车App测试版android6.0.1真机(红米Note3)[我的_个人主页的分享方式]测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
