#test purpose : verify the main features on XiaoMi
#os: Android
#device: Mi Redmi Note3
#version:android 6.0.1
#author: Sam Wang
#update date: created by Sam [2018-07-23]

#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestReportEN import HTMLTestRunner
#from appium.webdriver.common.touch_action import TouchAction

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class weilai_test(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['automationName'] = 'UIAutomator2'
        desired_caps['deviceName'] = 'kenzo'
        #desired_caps['udid'] = 'HMKNW17225011700'
        desired_caps['app'] = PATH('../weilai_test.apk')
        desired_caps['appPackage'] = 'cn.com.weilaihui3'
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        desired_caps['noReset'] = True

        self.driver = webdriver.Remote('http://127.0.0.1:4725/wd/hub', desired_caps)
        sleep(3)

    def tearDown(self):
        # end the session
        self.driver.quit()

    def findNotLoggedPlus(self):
        driver=self.driver
        print('TC_检查手机号码登录APP，检查点:发现_未登录账号发现首页点加号点发布此刻、发起群聊、扫一扫会进入登录界面')
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n发现_未登录账号发现首页点加号----开始:'+now)
        sleep(3)
        
        #我的
        driver.find_element_by_android_uiautomator('new UiSelector().text("我的")').click()
        sleep(2)
        driver.swipe(50,700,50,500,1000)
        sleep(1)
        #设置
        driver.find_element_by_id('cn.com.weilaihui3:id/my_palace_setting_layout').click()
        sleep(1)
        out=driver.find_elements_by_android_uiautomator('new UiSelector().text("退出登录")')
        if len(out)!=0:
            driver.find_element_by_android_uiautomator('new UiSelector().text("退出登录")').click()
            sleep(1)
            driver.find_element_by_android_uiautomator('new UiSelector().text("确定")').click()
            sleep(2)
        else:
            driver.find_element_by_id('cn.com.weilaihui3:id/navigation_back_icon').click()
            sleep(1)
        #发现
        driver.find_element_by_android_uiautomator('new UiSelector().text("发现")').click()
        sleep(2)
        #+
        driver.find_element_by_xpath('//android.widget.ImageView[@index="0"]').click()
        sleep(1)
        #发布此刻
        driver.find_element_by_android_uiautomator('new UiSelector().text("发布此刻")').click()
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_publishNow_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(1)
        button=driver.find_element_by_id('cn.com.weilaihui3:id/login_main_login_button')
        if '注册/登录' in button.text:
            print('\n未登录账号发现首页点加号发布此刻功能检查通过')
        else:
            print('\n未登录账号发现首页点加号发布此刻功能检查失败！')
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_back_icon').click()
        sleep(1)
        #+
        driver.find_element_by_xpath('//android.widget.ImageView[@index="0"]').click()
        sleep(1)
        #发起群聊
        driver.find_element_by_android_uiautomator('new UiSelector().text("发起群聊")').click()
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_openMultiChat_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(1)
        button=driver.find_element_by_id('cn.com.weilaihui3:id/login_main_login_button')
        if '注册/登录' in button.text:
            print('未登录账号发现首页点加号发起群聊功能检查通过')
        else:
            print('未登录账号发现首页点加号发起群聊功能检查失败！')
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_back_icon').click()
        sleep(1)
        #+
        driver.find_element_by_xpath('//android.widget.ImageView[@index="0"]').click()
        sleep(1)
        #扫一扫
        driver.find_element_by_android_uiautomator('new UiSelector().text("扫一扫")').click()
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_openMultiChat_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(1)
        button=driver.find_element_by_id('cn.com.weilaihui3:id/login_main_login_button')
        if '注册/登录' in button.text:
            print('未登录账号发现首页点加号扫一扫功能检查通过')
        else:
            print('未登录账号发现首页点加号扫一扫功能检查失败！')
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/navigation_back_icon').click()
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n发现_未登录账号发现首页点加号----结束:'+now)

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(weilai_test('findNotLoggedPlus'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_notLoggedPlus_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='蔚来汽车App测试版android6.0.1真机(红米Note3)[发现_未登录账号发现首页点加号]测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
