#test purpose : verify the main features on XiaoMi
#os: Android
#device: Mi Redmi Note3
#version:android 6.0.1
#author: Sam Wang
#update date: created by Sam [2018-07-20]

#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestReportEN import HTMLTestRunner
#from appium.webdriver.common.touch_action import TouchAction
#from pub_Teacher import login,logout,turnpage_play
from readAccount import getInfo

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class weilai_test(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['automationName'] = 'UIAutomator2'
        desired_caps['deviceName'] = 'kenzo'
        #desired_caps['udid'] = 'HMKNW17225011700'
        desired_caps['app'] = PATH('../weilai_test.apk')
        desired_caps['appPackage'] = 'cn.com.weilaihui3'
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        desired_caps['noReset'] = True

        self.driver = webdriver.Remote('http://127.0.0.1:4725/wd/hub', desired_caps)
        sleep(3)

    def tearDown(self):
        # end the session
        self.driver.quit()

    def myRelogin(self):
        driver=self.driver
        print('TC_检查手机号码登录APP，检查点:我的页面里重新登录账号，如果已登录先退出账号')
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n客户重新登录账号----开始:'+now)
        f=getInfo(self)
        sleep(5)
        #我的
        driver.find_element_by_android_uiautomator('new UiSelector().text("我的")').click()
        sleep(2)
        driver.swipe(50,700,50,500,1000)
        sleep(1)
        #设置
        driver.find_element_by_id('cn.com.weilaihui3:id/my_palace_setting_layout').click()
        sleep(1)
        out=driver.find_elements_by_android_uiautomator('new UiSelector().text("退出登录")')
        if len(out)!=0:
            driver.find_element_by_android_uiautomator('new UiSelector().text("退出登录")').click()
            sleep(1)
            driver.find_element_by_android_uiautomator('new UiSelector().text("确定")').click()
            sleep(2)
        else:
            driver.find_element_by_id('cn.com.weilaihui3:id/navigation_back_icon').click()
            sleep(1)
        driver.find_element_by_android_uiautomator('new UiSelector().text("注册/登录")').click()
        sleep(1)
        mobile_no=driver.find_element_by_id('cn.com.weilaihui3:id/login_main_login_phone_edit')
        mobile_no.click()
        #mobile_no.clear()
        #mobile_no.send_keys('98762648192')
        mobile_no.send_keys(f[0])
        sleep(1)
        #driver.press_keycode('66')
        #sleep(1)
        code=driver.find_element_by_id('cn.com.weilaihui3:id/login_main_login_code_edit')
        code.click()
        #code.send_keys('440697')
        code.send_keys(f[1])
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/login_main_login_button').click()
        sleep(5)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_relogin_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(2)
        name=driver.find_element_by_id('cn.com.weilaihui3:id/my_head_info_user_name')
        if 'Sam8198' in name.text:
            print('登录成功！')
        else:
            print('登录失败！')
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n客户重新登录账号----结束:'+now)

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(weilai_test('myRelogin'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_relogin_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='蔚来汽车App测试版android6.0.1真机(RedMi Note3)[我的_客户重新登录账号]测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
