#test purpose : verify the main features on XiaoMi
#os: Android
#device: Mi Redmi Note3
#version:android 6.0.1
#author: Sam Wang
#update date: created by Sam [2018-07-20]

#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestReportEN import HTMLTestRunner
#from appium.webdriver.common.touch_action import TouchAction

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class weilai_test(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['automationName'] = 'UIAutomator2'
        desired_caps['deviceName'] = 'kenzo'
        #desired_caps['udid'] = 'HMKNW17225011700'
        desired_caps['app'] = PATH('../weilai_test.apk')
        desired_caps['appPackage'] = 'cn.com.weilaihui3'
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        desired_caps['noReset'] = True

        self.driver = webdriver.Remote('http://127.0.0.1:4725/wd/hub', desired_caps)
        sleep(3)

    def tearDown(self):
        # end the session
        self.driver.quit()

    def myChangeIcon(self):
        driver=self.driver
        print('TC_检查手机号码登录APP，检查点:我的个人信息里头像的变更----拍照、从手机相册选择')
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n我的_个人信息改变账号头像----开始:'+now)
        sleep(3)
        #我的
        driver.find_element_by_android_uiautomator('new UiSelector().text("我的")').click()
        sleep(2)
        driver.find_element_by_id('cn.com.weilaihui3:id/my_head_info_user_head').click()
        sleep(2)
        #编辑个人信息
        driver.find_element_by_id('cn.com.weilaihui3:id/user_info_submit_btn').click()
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/login_update_user_message_header_image').click()
        sleep(1)
        #从手机相册选择
        driver.find_element_by_id('cn.com.weilaihui3:id/choose_bellow_button').click()
        sleep(2)
        driver.find_element_by_id('com.miui.gallery:id/pick_num_indicator').click()
        sleep(2)
        #确认
        driver.find_element_by_id('cn.com.weilaihui3:id/sure_clip_image_button').click()
        sleep(2)
        driver.find_element_by_android_uiautomator('new UiSelector().text("保存")').click()
        sleep(5)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_savedSelected_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(2)
        driver.find_element_by_id('cn.com.weilaihui3:id/user_info_submit_btn').click()
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/login_update_user_message_header_image').click()
        sleep(1)
        #拍照
        driver.find_element_by_android_uiautomator('new UiSelector().text("拍照")').click()
        sleep(2)
        driver.find_element_by_id('com.android.camera:id/v6_camera_picker').click()
        sleep(3)
        driver.find_element_by_id('com.android.camera:id/v6_shutter_button_internal').click()
        sleep(3)
        driver.find_element_by_id('com.android.camera:id/v6_btn_done').click()
        sleep(4)
        driver.find_element_by_id('cn.com.weilaihui3:id/sure_clip_image_button').click()
        sleep(3)
        driver.find_element_by_android_uiautomator('new UiSelector().text("保存")').click()
        sleep(5)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_savedSelfi_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(2)
        driver.press_keycode('4')
        sleep(2)
        driver.press_keycode('4')
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n我的_个人信息改变账号头像----结束:'+now)

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(weilai_test('myChangeIcon'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_changeIcon_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='蔚来汽车App测试版android6.0.1真机(红米Note3)[我的_个人信息改变账号头像]测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
