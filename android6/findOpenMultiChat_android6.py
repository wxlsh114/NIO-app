#os: Android
#device: Mi Redmi Note3
#version:android 6.0.1
#author: Sam Wang
#update date: created by Sam [2018-07-23]

#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestReportEN import HTMLTestRunner
#from appium.webdriver.common.touch_action import TouchAction

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class weilai_test(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['automationName'] = 'UIAutomator2'
        desired_caps['deviceName'] = 'kenzo'
        #desired_caps['udid'] = 'HMKNW17225011700'
        desired_caps['app'] = PATH('../weilai_test.apk')
        desired_caps['appPackage'] = 'cn.com.weilaihui3'
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        desired_caps['noReset'] = True

        self.driver = webdriver.Remote('http://127.0.0.1:4725/wd/hub', desired_caps)
        sleep(3)

    def tearDown(self):
        # end the session
        self.driver.quit()

    def findOpenMultiChat(self):
        driver=self.driver
        print('TC_检查手机号码登录APP，检查点:已登录账号发现首页_发起群聊功能')
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n发现_发起群聊----开始:'+now)
        sleep(3)
        #发现
        driver.find_element_by_android_uiautomator('new UiSelector().text("发现")').click()
        sleep(2)
        #+
        driver.find_element_by_xpath('//android.widget.ImageView[@index="0"]').click()
        sleep(2)
        #发起群聊
        driver.find_element_by_android_uiautomator('new UiSelector().text("发起群聊")').click()
        sleep(3)
        cb=driver.find_elements_by_id('cn.com.weilaihui3:id/cb')
        #print(len(cb))
        for i in range(len(cb)):
            driver.find_elements_by_class_name('android.widget.CheckBox')[i].click()
            sleep(1)
        driver.find_element_by_android_uiautomator('new UiSelector().text("确定")').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_openedMultiChat_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(1)
        msg=driver.find_element_by_id('cn.com.weilaihui3:id/rc_edit_text')
        msg.click()
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        msg.send_keys('Test:'+now)
        sleep(1)
        #发送
        driver.find_element_by_android_uiautomator('new UiSelector().text("发送")').click()
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_sentMessage_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(1)
        driver.find_element_by_id('cn.com.weilaihui3:id/toolbar_back_icon').click()
        sleep(1)
        #朋友
        driver.find_element_by_android_uiautomator('new UiSelector().text("朋友")').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_friends_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        driver.press_keycode('4')
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n发现_发起群聊----结束:'+now)

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(weilai_test('findOpenMultiChat'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_openMultiChat_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='蔚来汽车App测试版android6.0.1真机(红米Note3)[发现_发起群聊]测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
